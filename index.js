// 4. Import the http module using the required directive.
const http = require("http");

// 5. Create a variable port and assign it with the value of 3000.
let port = 3000;

// 6. Create a server using the createServer method that will listen in to the port provided above.
// 8. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
// 10. Create a condition for any other routes that will return an error message.

const server = http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('You are at the login page.');
	}
	else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not available. Please go to the login page!');
	}
});


// 7. Console log in the terminal a message when the server is successfully running.
server.listen(port);
console.log(`The server is now running at: ${port}`);

